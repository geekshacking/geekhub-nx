import {IsAlpha, IsAlphanumeric, IsUrl} from "class-validator";

export class CreateProjectRequestDto {
  @IsAlpha()
  key = ""

  @IsAlphanumeric()
  name = ""

  description = ""

  @IsUrl()
  repositoryUrl = ""
}
