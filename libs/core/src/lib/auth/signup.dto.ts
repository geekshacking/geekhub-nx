import {IsAlphanumeric, IsEmail, IsNotEmpty} from "class-validator";

export class SignupRequestDto {
  @IsEmail()
  email = ""

  @IsNotEmpty()
  firstName = ""

  @IsNotEmpty()
  lastName = ""

  @IsAlphanumeric()
  username = ""

  @IsNotEmpty()
  password = ""
}

export interface SignupResponseDto {
  success: boolean
}
