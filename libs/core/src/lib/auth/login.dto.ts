import {IsNotEmpty} from "class-validator";

export class LoginRequestDto {
  @IsNotEmpty()
  username = ""

  @IsNotEmpty()
  password = ""
}

export interface LoginResponseDto {
  success: boolean
}
