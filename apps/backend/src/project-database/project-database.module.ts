import {Module} from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";

import {ProjectDatabaseService} from './project-database.service';
import {PostgresProjectDatabaseService} from "./postgres/project-database.service";

import {Project} from "../project/project.entity";
import {Language} from "../project/language.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Project, Language])],
  providers: [{
    provide: ProjectDatabaseService,
    useClass: PostgresProjectDatabaseService
  }],
  exports: [{
    provide: ProjectDatabaseService,
    useClass: PostgresProjectDatabaseService
  }]
})

export class ProjectDatabaseModule {
}
