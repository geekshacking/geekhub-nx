import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

import {ProjectDatabaseService} from "../project-database.service";
import {Language} from "../../project/language.entity";
import {Project} from "../../project/project.entity";

@Injectable()
export class PostgresProjectDatabaseService implements ProjectDatabaseService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    @InjectRepository(Language)
    private readonly languageRepository: Repository<Language>
  ) {
  }

  async create(project: Project): Promise<Project> {
    return this.projectRepository.save(project)
  }

  findLanguageByName(name: string): Promise<Language> {
    return this.languageRepository.findOne({name})
  }

}
