import {Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {User} from "../user/user.entity";
import {Language} from "./language.entity";

@Entity()
export class Project {
  constructor(key: string, name: string, description: string, repositoryUrl: string) {
    this.key = key
    this.name = name
    this.description = description
    this.repositoryUrl = repositoryUrl
  }

  @PrimaryGeneratedColumn()
  public id: string

  @Column({nullable: false})
  public key: string

  @Column({nullable: false})
  public name: string

  @Column({nullable: false})
  public description: string

  @Column({nullable: false})
  public repositoryUrl: string

  @ManyToMany(() => User)
  public users: User[]

  @JoinTable()
  @ManyToMany(() => Language)
  public languages: Language[]
}
