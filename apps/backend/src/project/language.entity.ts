import {Column, Entity, ManyToMany, PrimaryGeneratedColumn} from "typeorm";
import {Project} from "./project.entity";

@Entity()
export class Language {
  constructor(name: string) {
    this.name = name
  }

  @PrimaryGeneratedColumn()
  public id: string

  @Column({nullable: false})
  public name: string

  @ManyToMany(() => Project, project => project.languages)
  public projects: Project[]
}
