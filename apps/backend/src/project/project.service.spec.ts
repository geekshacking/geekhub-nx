import {Test, TestingModule} from '@nestjs/testing';

import {ProjectService} from './project.service';
import {ProjectDatabaseModule} from "../project-database/project-database.module";
import {getRepositoryToken} from "@nestjs/typeorm";
import {Project} from "./project.entity";
import {Repository} from "typeorm";

export type MockType<T> = {
  [P in keyof T]?: jest.Mock<Project>;
};

export const repositoryMockFactory: () => MockType<Repository<Project>> = jest.fn(
  () => ({
    findOne: jest.fn((entity) => entity),
    create: jest.fn((entity) => entity.id),
    save: jest.fn((entity) => entity.id),
  }),
);

describe('ProjectService', () => {
  let service: ProjectService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ProjectDatabaseModule],
      providers: [ProjectService, {
        provide: getRepositoryToken(Project),
        useFactory: repositoryMockFactory
      }],
      exports: [ProjectService]
    }).compile();

    service = module.get<ProjectService>(ProjectService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
