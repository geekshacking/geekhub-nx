export class InvalidRepositoryUrlException extends Error {
  constructor() {
    super("Invalid repository url for project")
  }
}

export class RepositoryNotGitHubException extends Error {
  constructor() {
    super("Repository url is not a GitHub repository")
  }
}

export class InvalidGitHubRepositoryException extends Error {
  constructor() {
    super("Invalid GitHub repository")
  }
}
