import {Module} from '@nestjs/common';
import {ProjectService} from './project.service';
import {ProjectDatabaseModule} from "../project-database/project-database.module";

@Module({
  imports: [ProjectDatabaseModule],
  providers: [ProjectService],
  exports: [ProjectService]
})
export class ProjectModule {
}
