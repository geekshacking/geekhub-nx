import {Module} from '@nestjs/common';

import {TypeOrmModule} from "@nestjs/typeorm";
import {ConfigModule, ConfigService} from "@nestjs/config";

import {AppController} from './app.controller';
import {AppService} from './app.service';

import configuration from "../config/configuration";
import {AuthHttpModule} from "../auth-http/auth-http.module";
import {ProjectHttpModule} from "../project-http/project-http.module";

@Module({
  imports: [
    ConfigModule.forRoot({load: [configuration]}),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        type: configService.get<'postgres'>("db.type"),
        host: configService.get<string>("db.host"),
        port: configService.get<number>("db.port"),
        username: configService.get<string>("db.username"),
        password: configService.get<string>("db.password"),
        database: configService.get<string>("db.database"),
        synchronize: configService.get<boolean>("db.synchronize"),
        entities: [
          __dirname + '/../**/*.entity{.ts,.js}',
        ],
        autoLoadEntities: true
      }),
    }),

    AuthHttpModule,
    ProjectHttpModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
