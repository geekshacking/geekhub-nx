import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  Get,
  NotFoundException,
  Post,
  Req,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import {
  LoginRequestDto,
  LoginResponseDto,
  SignupRequestDto,
  SignupResponseDto,
  WhoAmIResponseDto
} from "@geekhub-nx/core";
import {AuthService} from "../auth/auth.service";
import {
  BadPasswordException,
  InvalidTokenException,
  UsernameExistsException,
  UsernameNotFoundException
} from "../auth/exceptions";

import {Request, Response} from "express"

@Controller('auth')
export class AuthHttpController {
  constructor(private readonly authService: AuthService) {
  }

  @Post("signup")
  public async signup(
    @Body() {email, firstName, lastName, username, password}: SignupRequestDto,
  ): Promise<SignupResponseDto> {
    try {
      await this.authService.signup(email, username, firstName, lastName, password)
      return {
        success: true
      }
    } catch (e) {
      if (e instanceof UsernameExistsException) {
        throw new ConflictException()
      }
      throw e
    }
  }

  @Post("login")
  public async login(
    @Body() {username, password}: LoginRequestDto,
    @Res({passthrough: true}) res: Response
  ): Promise<LoginResponseDto> {
    try {
      const token = await this.authService.login(username, password)
      res.cookie("t", token, {httpOnly: true})

      return {success: true}
    } catch (e) {
      if (e instanceof UsernameNotFoundException) {
        throw new NotFoundException()
      } else if (e instanceof BadPasswordException) {
        throw new BadRequestException()
      }

      throw e
    }
  }

  @Get("whoami")
  public async whoami(
    @Req() req: Request
  ): Promise<WhoAmIResponseDto> {
    if (!req.cookies["t"]) {
      throw new UnauthorizedException()
    }

    try {
      const username = await this.authService.subject(req.cookies["t"])
      return {username}
    } catch (e) {
      if (e instanceof InvalidTokenException) {
        throw new UnauthorizedException()
      }
    }
  }

  @Get("refresh")
  public async refresh(
    @Req() req: Request,
    @Res({passthrough: true}) res: Response
  ): Promise<{ success: boolean }> {
    if (!req.cookies["t"]) {
      throw new UnauthorizedException()
    }

    try {
      const token = await this.authService.refresh(req.cookies["t"])
      res.cookie("t", token, {httpOnly: true})
      return {success: true}
    } catch (e) {
      if (e instanceof InvalidTokenException) {
        throw new UnauthorizedException()
      }
    }
  }
}
