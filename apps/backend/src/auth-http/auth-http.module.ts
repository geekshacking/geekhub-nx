import { Module } from '@nestjs/common';
import { AuthHttpController } from './auth-http.controller';
import {AuthModule} from "../auth/auth.module";

@Module({
  imports: [AuthModule],
  controllers: [AuthHttpController],
})
export class AuthHttpModule {}
