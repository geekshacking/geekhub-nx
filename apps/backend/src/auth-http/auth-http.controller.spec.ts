import {Test, TestingModule} from '@nestjs/testing';
import {AuthHttpController} from './auth-http.controller';
import {AuthModule} from "../auth/auth.module";

describe('AuthHttpController', () => {
  let controller: AuthHttpController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AuthModule],
      controllers: [AuthHttpController],
    }).compile();

    controller = module.get<AuthHttpController>(AuthHttpController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
