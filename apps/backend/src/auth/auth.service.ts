import {Injectable, Logger} from '@nestjs/common';
import {ConfigService} from "@nestjs/config";

import {compare, hash} from "bcrypt"
import {sign, verify} from "jsonwebtoken"

import {User} from "../user/user.entity";
import {AuthDatabaseService} from "../auth-database/auth-database.service";
import {
  BadPasswordException,
  InvalidTokenException,
  UsernameExistsException,
  UsernameNotFoundException
} from "./exceptions";


const saltRounds = 10;

@Injectable()
export class AuthService {
  constructor(
    private readonly authDatabaseService: AuthDatabaseService,
    private readonly configService: ConfigService
  ) {
  }

  /**
   * Signup creates a new user and returns a User object
   * @param email
   * @param username
   * @param firstName
   * @param lastName
   * @param password
   */
  public async signup(email: string, username: string, firstName: string, lastName: string, password: string): Promise<User> {
    const exists = await this.authDatabaseService.findByUsername(username)
    if (exists) throw new UsernameExistsException()

    const passwordHash = await hash(password, saltRounds)
    const user = new User(email, username, firstName, lastName, passwordHash)

    return await this.authDatabaseService.create(user)
  }

  /**
   * Login validates a users credentials and returns a JWT string
   * @param username
   * @param password
   */
  public async login(username: string, password: string): Promise<string> {
    const user = await this.authDatabaseService.findByUsername(username)
    if (!user) throw new UsernameNotFoundException()

    const validPassword = await compare(password, user.password)
    if (!validPassword) throw new BadPasswordException()

    return sign({}, this.configService.get<string>("jsonwebtoken.secret"), {
      notBefore: 0,
      expiresIn: "30m",
      issuer: "geekhub-nx-backend",
      audience: "geekhub-nx-frontend",
      subject: username
    })
  }

  /**
   * Subject returns the subject of a token if it is validd
   * @param token
   */
  public async subject(token: string): Promise<string> {
    let sub;
    verify(token, this.configService.get<string>("jsonwebtoken.secret"), {
      issuer: "geekhub-nx-backend",
      audience: "geekhub-nx-frontend"
    }, (err, decoded) => {
      if (err) {
        Logger.error(err)
        throw new InvalidTokenException()
      }

      sub = decoded.sub
    })

    return sub
  }

  public async refresh(old: string): Promise<string> {
    let sub;
    verify(old, this.configService.get<string>("jsonwebtoken.secret"), {
      ignoreExpiration: true,
      issuer: "geekhub-nx-backend",
      audience: "geekhub-nx-frontend"
    }, (err, decoded) => {
      if (err) {
        Logger.error(err)
        throw new InvalidTokenException()
      }

      sub = decoded.sub
    })

    return sign({}, this.configService.get<string>("jsonwebtoken.secret"), {
      notBefore: 0,
      expiresIn: "30m",
      issuer: "geekhub-nx-backend",
      audience: "geekhub-nx-frontend",
      subject: sub
    })
  }
}
