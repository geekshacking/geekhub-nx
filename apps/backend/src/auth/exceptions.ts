export class UsernameExistsException extends Error {
  constructor() {
    super("Username exists")
  }
}

export class UsernameNotFoundException extends Error {
  constructor() {
    super("Username not found")
  }
}

export class BadPasswordException extends Error {
  constructor() {
    super("Bad password exception")
  }
}

export class InvalidTokenException extends Error {
  constructor() {
    super("Invalid token")
  }
}
