import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

import {AuthDatabaseService} from "../auth-database.service";
import {User} from "../../user/user.entity";

@Injectable()
export class PostgresAuthDatabaseService implements AuthDatabaseService {
  constructor(@InjectRepository(User) private readonly usersRepository: Repository<User>) {
  }

  create(user: User): Promise<User> {
    return this.usersRepository.save(user)
  }

  findByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({email})
  }

  findByUsername(username: string): Promise<User> {
    return this.usersRepository.findOne({username})
  }
}
