import {Module} from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";

import {PostgresAuthDatabaseService} from "./postgres/auth-database.service";
import {AuthDatabaseService} from './auth-database.service';
import {User} from "../user/user.entity";

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [{
    provide: AuthDatabaseService,
    useClass: PostgresAuthDatabaseService
  }],
  exports: [{
    provide: AuthDatabaseService,
    useClass: PostgresAuthDatabaseService
  }],
})
export class AuthDatabaseModule {
}
