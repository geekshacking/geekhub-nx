import {User} from "../user/user.entity";

export abstract class AuthDatabaseService {
  abstract findByUsername(username: string): Promise<User>

  abstract findByEmail(email: string): Promise<User>

  abstract create(user: User): Promise<User>
}
