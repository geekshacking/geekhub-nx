import {Test, TestingModule} from '@nestjs/testing';
import {ProjectHttpController} from './project-http.controller';
import {ProjectModule} from "../project/project.module";

describe('ProjectHttpController', () => {
  let controller: ProjectHttpController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ProjectModule],
      controllers: [ProjectHttpController],
    }).compile();

    controller = module.get<ProjectHttpController>(ProjectHttpController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
