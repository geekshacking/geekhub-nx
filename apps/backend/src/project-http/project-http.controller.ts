import {BadRequestException, Body, Controller, Post} from '@nestjs/common';
import {CreateProjectRequestDto} from "@geekhub-nx/core";
import {ProjectService} from "../project/project.service";
import {InvalidRepositoryUrlException, RepositoryNotGitHubException} from "../project/exceptions";

@Controller('projects')
export class ProjectHttpController {
  constructor(private readonly projectService: ProjectService) {
  }

  @Post()
  public async create(
    @Body() {key, name, description, repositoryUrl}: CreateProjectRequestDto
  ): Promise<void> {
    try {
      await this.projectService.create(key, name, description, repositoryUrl)
    } catch (e) {
      if (e instanceof InvalidRepositoryUrlException) {
        throw new BadRequestException("Invalid repository url")
      } else if (e instanceof RepositoryNotGitHubException) {
        throw new BadRequestException("Repository url must be a GitHub repository")
      }

      throw e
    }
  }

}
