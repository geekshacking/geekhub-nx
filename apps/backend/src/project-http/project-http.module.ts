import {Module} from '@nestjs/common';
import {ProjectHttpController} from './project-http.controller';
import {ProjectModule} from "../project/project.module";

@Module({
  imports: [ProjectModule],
  controllers: [ProjectHttpController],
})
export class ProjectHttpModule {
}
