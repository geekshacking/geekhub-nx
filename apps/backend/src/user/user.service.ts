import {Repository} from "typeorm";
import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";

import {User} from "./user.entity";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {
  }

  findOne(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  create(user: User): Promise<User> {
    return this.usersRepository.save(user)
  }

  async remove(id: number): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
