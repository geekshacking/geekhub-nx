import {Test, TestingModule} from '@nestjs/testing';
import {TypeOrmModule} from "@nestjs/typeorm";

import {UserService} from './user.service';
import {User} from "./user.entity";

import {UserController} from "./user.controller";

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TypeOrmModule.forFeature([User])],
      providers: [UserService],
      controllers: [UserController],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
