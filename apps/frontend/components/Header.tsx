import {Button, Container, Group, Header as MantineHeader, Title} from "@mantine/core";
import {useRouter} from "next/router";

export interface HeaderProps {
  username?: string
}

export function Header(props: HeaderProps) {
  const router = useRouter()

  const onClickHome = () => router.push("/")
  const onClickLogin = () => router.push("/auth/login")
  const onClickSignup = () => router.push("/auth/signup")

  return (
    <MantineHeader
      height={60}
      sx={theme => ({
        display: "flex",
        alignItems: "center"
      })}
    >
      <div style={{flex: 1}}>
        <Container sx={{justifyContent: "space-between", display: "flex"}}>
          <Title order={3} sx={{cursor: "pointer"}} onClick={onClickHome}>GeekHub</Title>
          <Group spacing="sm">
            {props.username ? (
              <Button variant={"light"}>My Account</Button>
            ) : (
              <>
                <Button variant={"light"} onClick={onClickLogin}>Login</Button>
                <Button onClick={onClickSignup}>Sign up</Button>
              </>
            )}
          </Group>
        </Container>
      </div>
    </MantineHeader>
  )
}
