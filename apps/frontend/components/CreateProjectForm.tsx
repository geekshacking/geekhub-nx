import {useState} from "react"

import {useForm} from "@mantine/hooks";
import {Box, Button, Group, TextInput} from "@mantine/core";

import {ky} from "../api/ky";

export function CreateProjectForm() {
  const [loading, setLoading] = useState(false)

  const form = useForm({
    initialValues: {
      key: "",
      name: "",
      description: "",
      repositoryUrl: ""
    },

    validationRules: {
      key: (value) => value.length > 0,
      name: (value) => value.length > 0,
      repositoryUrl: (value) => value.indexOf("github") > -1 && new RegExp(/https:\/\/github.com\/.+\/.+/g).test(value)
    },

    errorMessages: {
      key: "Please input a valid key",
      name: "Please input a valid name",
      repositoryUrl: "Please input a GitHub repository url"
    }
  })

  async function create({key, name, description, repositoryUrl}) {
    setLoading(true)
    try {
      await ky.post("projects", {json: {key, name, description, repositoryUrl}}).json()
    } catch (e) {
      console.log(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <form onSubmit={form.onSubmit(create)}>
      <Group spacing={"md"} direction={"column"} grow>
        <TextInput
          label={"Project Key"}
          placeholder="APPL"
          error={form.errors["key"]}
          {...form.getInputProps("key")}
        />
        <TextInput
          label={"Name"}
          placeholder="Apple"
          error={form.errors["name"]}
          {...form.getInputProps("name")}
        />
        <TextInput
          label={"Repository URL"}
          placeholder="https://github.com/geekshacking/geekhub"
          error={form.errors["repositoryUrl"]}
          {...form.getInputProps("repositoryUrl")}
        />
        <Box sx={{display: "flex", justifyContent: "flex-end"}}>
          <Button type={"submit"} loading={loading}>
            Create it now!
          </Button>
        </Box>
      </Group>
    </form>
  )
}
