import {useState} from "react";

import {HTTPError} from "ky";
import {useRouter} from "next/router";

import {useForm} from "@mantine/hooks";
import {Box, Button, Card, Center, Group, PasswordInput, TextInput, Title} from "@mantine/core";

import {Header} from "../../components/Header";
import {ky} from "../../api/ky";
import {useWhoAmI} from "../../hooks/use-who-am-i";

export default function Signup() {
  const router = useRouter()

  const {isLoading, data} = useWhoAmI()
  const [loading, setLoading] = useState(false)

  const form = useForm({
    initialValues: {
      email: "",
      firstName: "",
      lastName: "",
      username: "",
      password: ""
    },

    validationRules: {
      email: (value) => /^\S+@\S+$/.test(value),
      firstName: (value) => value.length > 0,
      lastName: (value) => value.length > 0,
      username: (value) => value.length > 0,
      password: (value) => value.length > 0,
    },

    errorMessages: {
      email: "Please enter a valid email",
      firstName: "Please enter a valid first name",
      lastName: "Please enter a valid last name",
      username: "Please enter a valid username",
      password: "Please enter a valid password"
    }
  });

  if (!isLoading && data) {
    router.push("/")
    return null
  }

  const signup = async ({email, firstName, lastName, username, password}) => {
    setLoading(true)
    try {
      await ky.post("auth/signup", {json: {email, firstName, lastName, username, password}})
      await ky.post("auth/login", {json: {username, password}})
      router.push("/")
    } catch (e) {
      if (e instanceof HTTPError && (e as HTTPError).response.status === 409) {
        form.setFieldError("username", "This username already exists")
      }
    } finally {
      setLoading(false)
    }
  }

  return (
    <Box sx={{height: "100vh", display: "flex", flexDirection: "column"}}>
      <Header/>
      <Center sx={{flex: 1}}>
        <Card padding={"xl"} sx={{
          minWidth: "300px",
        }}>
          <Title order={3} mb={"sm"}>Sign up</Title>
          <form onSubmit={form.onSubmit(signup)}>
            <Group spacing={"md"} direction={"column"} grow>
              <TextInput
                label={"Email"}
                placeholder="johnappleseed@apple.com"
                error={form.errors["email"]}
                {...form.getInputProps("email")}
              />
              <TextInput
                label={"First name"}
                placeholder="John"
                error={form.errors["firstName"]}
                {...form.getInputProps("firstName")}
              />
              <TextInput
                label={"Last name"}
                placeholder="Appleseed"
                error={form.errors["lastName"]}
                {...form.getInputProps("lastName")}
              />
              <TextInput
                label={"Username"}
                placeholder="johnappleseed"
                error={form.errors["username"]}
                {...form.getInputProps("username")}
              />
              <PasswordInput
                label={"Password"}
                placeholder="Password"
                {...form.getInputProps("password")}
              />
              <Button type={"submit"} loading={loading}>Sign up</Button>
            </Group>
          </form>
        </Card>
      </Center>
    </Box>
  )
}
