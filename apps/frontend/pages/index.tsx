import {useRouter} from "next/router";
import {AppShell, Box, Button, Loader, Modal, Navbar, Title} from "@mantine/core";
import {useViewportSize} from "@mantine/hooks";

import {useWhoAmI} from "../hooks/use-who-am-i";
import {Header} from "../components/Header";
import {useState} from "react";
import {CreateProjectForm} from "../components/CreateProjectForm";

export default function Index() {
  const router = useRouter()
  const {height} = useViewportSize()

  const [createProjectModelOpen, setCreateProjectModelOpen] = useState<boolean>(false)

  const {isLoading, error, data} = useWhoAmI()

  if (isLoading) {
    return (
      <Box sx={{height: "100vh", display: "flex", flexDirection: "column"}}>
        <Loader/>
      </Box>
    )
  }

  if (error) {
    router.push("/about")
    return null
  }

  function open() {
    setCreateProjectModelOpen(true)
  }

  function close() {
    setCreateProjectModelOpen(false)
  }

  return (
    <AppShell
      padding="md"
      navbar={
        <Navbar width={{base: 300}} height={height - 60} padding="lg">
          <Navbar.Section>
            <Box sx={{display: "flex", justifyContent: "space-between", alignItems: "center"}}>
              <Title order={4}>Your projects</Title>
              <Button variant={"outline"} onClick={open}>New</Button>
            </Box>
          </Navbar.Section>
        </Navbar>
      }
      header={
        <Header username={data.username}/>
      }
      styles={(theme) => ({
        main: {backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0]},
      })}
    >
      <Modal
        size={"lg"}
        centered

        title={<Title order={3}>Start an amazing project</Title>}

        opened={createProjectModelOpen}
        onClose={close}
      >
        <CreateProjectForm/>
      </Modal>
    </AppShell>
  )
}
