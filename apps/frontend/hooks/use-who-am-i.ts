import {ky} from "../api/ky"
import {useQuery} from "react-query";
import {WhoAmIResponseDto} from "@geekhub-nx/core";

export function useWhoAmI() {
  return useQuery<WhoAmIResponseDto>("auth/whoami", async () =>
    await ky.get("auth/whoami").json<WhoAmIResponseDto>()
  )
}
